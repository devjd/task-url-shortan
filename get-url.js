'use strict';

module.exports.handler = async (event) => {
  const response = {
      statusCode: 200,
      headers: {
          "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
          "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
      },
      body:JSON.stringify({
        'message':"Something went wrong!",
      })
  };
  if (event.body !== null && event.body !== undefined) {
      // CREATE FORMDATA ARRAY FROM RAW DATA 
      var formData = JSON.parse(event.body);
      // VALIDATE IF URL ADDED OR NOT
      if(formData.slug==null || formData.slug==undefined){
        response.statusCode = 204;
        response.body = JSON.stringify({
          'message':"No slug found to process!",
        });
        return response;
      }
      var AWS = require('aws-sdk');
      AWS.config.update({region: "us-east-2" });
      var docClient = new AWS.DynamoDB.DocumentClient();
      
      // CREATE PARAMETERS 
      let slug = formData.slug;
      
      // TABLE PARAMETER
      var params = {
          TableName : "pracTestShortanUrls",
          Key: {
              "slug": slug,
          }
      };
      
      // INSERT RECORDS INTO THE TABLE
      try {
        const data = await docClient.get(params).promise();
        response.statusCode = 200;
        response.body = JSON.stringify({
          'message':"Fetched url successfully!",
          'data': { params, data }
        });
        return response;
      } catch (error) { 
        return response;
      }
  }else{
      return response;
  }
  
};
