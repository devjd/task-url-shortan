'use strict';

// FUNCTION TO GENERATE RANDOM STRING
function makeid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

module.exports.handler = async (event) => {
  const response = {
      statusCode: 200,
      headers: {
          "Access-Control-Allow-Origin" : "*", // Required for CORS support to work
          "Access-Control-Allow-Credentials" : true // Required for cookies, authorization headers with HTTPS 
      },
      body:JSON.stringify({
        'message':"Something went wrong!",
      })
  };
  if (event.body !== null && event.body !== undefined) {
      // CREATE FORMDATA ARRAY FROM RAW DATA 
      var formData = JSON.parse(event.body);
      // VALIDATE IF URL ADDED OR NOT
      if(formData.longUrl==null || formData.longUrl==undefined){
        response.statusCode = 204;
        response.body = JSON.stringify({
          'message':"No URL found to process!",
        });
        return response;
      }
      var AWS = require('aws-sdk');
      AWS.config.update({region: "us-east-2" });
      var docClient = new AWS.DynamoDB.DocumentClient();
      
      // CREATE PARAMETERS 
      let slug = makeid(6);
      let longUrl = formData.longUrl;
      
      // TABLE PARAMETER
      var params = {
          TableName : "pracTestShortanUrls",
          Item: {
              "slug": slug,
              "longUrl": longUrl
          }
      };
      
      // INSERT RECORDS INTO THE TABLE
      try {
        const data = await docClient.put(params).promise();
        response.statusCode = 200;
        response.body = JSON.stringify({
          'message':"Created url successfully!",
          'data': { params, data }
        });
        return response;
      } catch (error) { 
        return response;
      }
  }else{
      return response;
  }
  
};
