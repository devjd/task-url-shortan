1. Serverless framework to locally implement project and deploy over AWS 
2. Lambda functions are developed using Node.js. back-end logic for functionality is written in lambda.
3. home.html is index.html hosted in s3 static site. All code for creating and redirecting short URL is written in it. 
4. Serverless offline was the plugin that I used for local development 
5. Data is stored and used using Dynamo DB
6. Web UI is hosted on s3 static hosting