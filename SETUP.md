- Code repository is hosted on bitbucket. 
     - git clone https://devjd@bitbucket.org/devjd/task-url-shortan.git
- you will need to add AWS_ID & SECRET_ACCESS_KEY of your AWS account. That account must have admin level rights.

Fire below commands and create your project and then copy cloned code.

1. npm init
2. npm install -g serverless
3. serverless -v
4. npm install
5. serverless deploy